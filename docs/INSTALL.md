# build
sudo apt install qtbase5-dev qtdeclarative5-dev libmpv-dev build-essential

# runtime
sudo apt install qml-module-qtqml qml-module-qtquick2 qml-module-qtquick-controls qml-module-qtquick-controls2 qml-module-qtquick-window2