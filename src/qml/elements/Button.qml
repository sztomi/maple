import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Controls.Styles 1.4
import "."

Button {
  palette.button: Style.colors.blueMunsell
  palette.buttonText: Style.colors.white
}